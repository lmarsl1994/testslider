
var slideNum = 0;
var roundsBox = document.querySelector('.slider-rounds')
var rounds = document.querySelectorAll('.slide')


rounds.forEach(function (el, idx) {
    var elRound = document.createElement('span')
    elRound.className = 'slider-rounds_item'
    elRound.addEventListener('click', function () {
        showSlides(slideNum = (idx));
    })
    roundsBox.append(elRound)
})

showSlides(slideNum);

function nextSlide() {
    showSlides(slideNum += 1);

}

function prevSlide() {
    showSlides(slideNum -= 1);
}


function showSlides(n) {
    console.log(n)
    var elSlides = document.getElementsByClassName("slide");
    var elRounds = document.getElementsByClassName("slider-rounds_item");
    if (n > (elSlides.length-1)) {
        slideNum = 0
    }
    if (n < 0) {
        slideNum = (elSlides.length-1)
    }
    hideSlides(elSlides)

    clearRounds(elRounds)

    elSlides[slideNum].style.display = "flex";
    elRounds[slideNum].className += " active";
}

function hideSlides(el){
    var i;
    for (i = 0; i < el.length; i++) {
        el[i].style.display = "none";
    }
}
function clearRounds(el){
    var i;
    for (i = 0; i < el.length; i++) {
        el[i].className = el[i].className.replace(" active", "");
    }
}